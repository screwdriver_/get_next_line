/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 17:31:36 by llenotre          #+#    #+#             */
/*   Updated: 2018/11/20 14:09:54 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_pow(int n, unsigned int factor)
{
	unsigned int	i;
	int				j;

	if (factor == 0)
		return (1);
	i = 0;
	j = n;
	while (i < factor)
	{
		j *= n;
		++i;
	}
	return (j);
}
